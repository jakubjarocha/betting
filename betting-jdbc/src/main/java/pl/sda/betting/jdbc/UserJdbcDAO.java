package pl.sda.betting.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import pl.sda.betting.domain.entity.User;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class UserJdbcDAO {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public UserJdbcDAO(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final String SQL_FIND_BY_ID = "SELECT USER_ID, FIRST_NAME, LAST_NAME, BIRTHDATE FROM USER WHERE USER_ID = ?";
    private static final String SQL_FIND_ALL = "SELECT USER_ID, FIRST_NAME, LAST_NAME, BIRTHDATE FROM USER";

    public User findById(Integer id) {
        return this.jdbcTemplate.queryForObject(SQL_FIND_BY_ID, new Object[]{id},
                new RowMapper<User>() {
                    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return mapToUser(rs);
                    }
                });
    }

    public List<User> findAll() {
        return this.jdbcTemplate.query(SQL_FIND_ALL,
                new RowMapper<User>() {
                    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return mapToUser(rs);
                    }
                });
    }

    private User mapToUser(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId((int) resultSet.getLong("USER_ID"));
        user.setFirstName(resultSet.getString("FIRST_NAME"));
        user.setLastName(resultSet.getString("LAST_NAME"));
        user.setBirthdate(resultSet.getDate("BIRTHDATE"));
        return user;
    }
}
