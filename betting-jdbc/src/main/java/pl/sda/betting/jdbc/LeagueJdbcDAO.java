package pl.sda.betting.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import pl.sda.betting.domain.entity.League;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
@Repository
public class LeagueJdbcDAO {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public LeagueJdbcDAO(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final String SQL_FIND_BY_ID = "SELECT ID_LEAGUE, LEAGUE, DISCIPLINE, TEAM_OR_PLAYER FROM LEAGUE WHERE ID_LEAGUE = ?";
    private static final String SQL_FIND_ALL = "SELECT ID_LEAGUE, LEAGUE, DISCIPLINE, TEAM_OR_PLAYER FROM LEAGUE";

    public League findById(Long id) {
        return this.jdbcTemplate.queryForObject(SQL_FIND_BY_ID, new Object[]{id},
                new RowMapper<League>() {
                    public League mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return mapToLeague(rs);
                    }
                });
    }

    public List<League> findAll() {
        return this.jdbcTemplate.query(SQL_FIND_ALL,
                new RowMapper<League>() {
                    public League mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return mapToLeague(rs);
                    }
                });
    }

    private League mapToLeague(ResultSet resultSet) throws SQLException {
        League league = new League();
        league.setId(resultSet.getLong("ID_LEAGUE"));
        league.setLeagueName(resultSet.getString("LEAGUE"));
        league.setDiscipline(resultSet.getString("DISCIPLINE"));
        league.setTeamOrplayer(resultSet.getString("TEAM_OR_PLAYER"));
        return league;
    }
}
