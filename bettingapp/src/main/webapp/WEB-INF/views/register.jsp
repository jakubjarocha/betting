<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%--
  Created by IntelliJ IDEA.
  User: RENT
  Date: 2017-11-07
  Time: 19:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>betting</title>
</head>
<body>

<h2><spring:message code="application.title"/></h2>
<spring:message code="lang.change"/>
<a href="?language=en"><spring:message code="lang.eng"/></a>|
<a href="?language=pl"><spring:message code="lang.pl"/></a>

<h3><spring:message code="user.list.title"/></h3>

<table class="tg">
    <tr>
        <th width="200"><spring:message code="user.list.label.firstName"/></th>
        <th width="200"><spring:message code="user.list.label.lastName"/></th>
        <th width="210"><spring:message code="user.list.label.birthdate"/></th>
        <th width="210"><spring:message code="user.list.label.email"/></th>

    </tr>
    <c:choose>
        <c:when test="${!empty listUser}">
            <c:forEach items="${listUser}" var="user">
                <tr>
                    <td>${user.firstName}</td>
                    <td>${user.lastName}</td>
                    <td>${user.birthdate}</td>
                    <td>${user.email}</td>
                    <td>
                        <a href="<c:url value='/user/edit/${user.id}'/>">
                            <spring:message code="user.list.action.edit"/>
                        </a>
                    </td>
                </tr>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <tr>
                <td><spring:message code="user.list.message.empty"/></td>
            </tr>
        </c:otherwise>
    </c:choose>
</table>
<fieldset>
    <legend><spring:message code="registration.title" /></legend>
    <h3><spring:message code="user.form.title"/></h3>

    <c:url var="saveAction" value="/user/save"/>

    <form:form method="post" modelAttribute="user" action="${saveAction}">
    <table>
        <tr>
            <td colspan="2">
                <form:hidden path="id"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="firstName">
                    <spring:message code="user.form.label.firstName"/>
                </form:label>
            </td>
            <td>
                <form:input path="firstName"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="lastName">
                    <spring:message code="user.form.label.lastName"/>
                </form:label>
            </td>
            <td>
                <form:input path="lastName"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="birthdate">
                    <spring:message code="user.form.label.birthdate"/>
                </form:label>
            </td>
            <td>
                <form:input type="date" path="birthdate"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="email">
                    <spring:message code="user.form.label.email"/>
                </form:label>
            </td>
            <td>
                <form:input path="email"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <spring:message code="user.form.submit.label" var="labelSubmit"/>
                <input name="submit" type="submit" value="${labelSubmit}"/>
            </td>
        </tr>
    </table>
    </fieldset>
    </form:form>
</body>
</html>
