<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>betting</title>
</head>
<body>
<div>
<spring:message code="logout.submit.label" var="labelSubmit"/>

<form:form method="post" action="${logoutUrl}">
    <input type="submit"
           value="${labelSubmit}"/>
</form:form>

<h3><spring:message code="user.list.title"/></h3>

</div>
</body>
</html>
