<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>betting</title>
</head>
<body bgcolor="#a9a9a9">
<center><b><h2><spring:message code="application.title"/></h2></b></center>
<marquee><h3><spring:message code="application.title2"/></h3></marquee>

<p align="right"><fmt:formatDate value="${bean.date}" pattern="yyyy-MM-dd HH:mm:ss" /></p>
<spring:message code="lang.change"/>
<a href="?language=en"><spring:message code="lang.eng"/></a>|
<a href="?language=pl"><spring:message code="lang.pl"/></a>
<fieldset>
    <legend><spring:message code="login.title" /></legend>
    <form name='loginForm' action="perform_login" method='POST'>
        <table>
            <tr>
                <td><spring:message code="login.label.user"/></td>
                <td>
                    <input type='text' name='username' value=''>
                </td>
            </tr>
            <tr>
                <td><spring:message code="login.label.password"/></td>
                <td>
                    <input type='password' name='password'/>
                </td>
            </tr>
            <tr>
                <td colspan='2'>
                    <a href="<c:url value='/admin'/>">
                        <spring:message code="login.submit.label"/>
                    </a>
                </td>
            </tr>
        </table>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
</fieldset>
<a href="register/index.html"><spring:message code="login.label.newUser"/></a>|
<a href="forgotPassword/index.html"><spring:message code="login.label.forgotPassword"/></a>|
</body>
</html>