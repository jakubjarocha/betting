<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>library</title>
</head>
<body>


        <h1>Administrator Control Panel</h1>
        <a href="<c:url value='/admin'/>">
            Profil Administratora
        </a>

<c:url var="saveAction" value="/admin/saveLeague"/>
<form:form method="post" modelAttribute="league" action="${saveAction}">

        <fieldset>
            <legend>Dodaj Ligę</legend>

            <form:label path="leagueName"> Nazwa Ligi: </form:label> <form:input path="leagueName"/> <br/>
            <form:label path="discipline"> Dyscyplina: </form:label>  <form:input path="discipline"/> <br/>
            <form:label path="teamOrplayer"> Drużyna/Zawodnik: </form:label><form:input type="path" path="teamOrplayer"/> <br/>

            <spring:message code="league.add" var="labelSubmit"/>
            <input type="submit" value="labelSubmit" value="${labelSubmit}"/>
        </fieldset>
</form:form>
<br/>
        <h3>Lista zapisanych lig</h3>
        <table class="tg">
            <tr>
                <th width="200">Liga</th>
                <th width="200">Dyscyplina</th>
                <th width="210">Drużyna/Zawodnik</th>
            </tr>
            <c:choose>
                <c:when test="${!empty listLeague}">
                    <c:forEach items="${listLeague}" var="league">
                        <tr>
                            <td>${league.leagueName}</td>
                            <td>${league.discipline}</td>
                            <td>${league.teamOrplayer}</td>
                            <td>
                                <a href="<c:url value='/league/edit/${league.id}'/>">
                                    edytuj
                                </a>
                            </td>
                            <td>
                                <a href="<c:url value='/league/delete/${league.id}'/>">
                                    usun
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <tr>
                        <td>brak lig</td>
                    </tr>
                </c:otherwise>
            </c:choose>
        </table>


</body>
</html>
