package pl.sda.betting.app.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.sda.betting.domain.entity.League;
import pl.sda.betting.service.league.command.LeagueCommandService;
import pl.sda.betting.service.league.query.LeagueQueryService;

import javax.validation.Valid;

@Controller
public class LeagueSpringControler {
    private static final Logger LOGGER = LoggerFactory.getLogger(LeagueSpringControler.class);

    private final LeagueCommandService leagueCommandService;
    private final LeagueQueryService leagueQueryService;

    @Autowired
    public LeagueSpringControler(LeagueCommandService leagueCommandService, LeagueQueryService leagueQueryService) {
        this.leagueCommandService = leagueCommandService;
        this.leagueQueryService = leagueQueryService;
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String adminProfile() {

        return "adminProfile";
    }

    @RequestMapping(value = "/adminAdd", method = RequestMethod.GET)
    public String adminAddLeaguePanel(@ModelAttribute("flashAccount") League league, Model model) {
        LOGGER.debug("is executed!");
        model.addAttribute("listLeague", leagueQueryService.findAll());
        model.addAttribute("league", league);

        return "admin-AddLeague";
    }

    @RequestMapping(value = "/admin/saveLeague", method = RequestMethod.POST)
    public String saveLeague(@Valid @ModelAttribute("league") League league,
                             BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        LOGGER.debug("is executed!");

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.league", bindingResult);
            redirectAttributes.addFlashAttribute("flashLeague", league);

            return "redirect:/adminAdd";
        }

        if (league.getId() == null) {
            leagueCommandService.create(league);
        } else {
            leagueCommandService.update(league);
        }

        return "redirect:/adminAdd";
    }

    @RequestMapping(value = "/league/edit/{id}", method = RequestMethod.GET)
    public String editLeague(@PathVariable("id") Long id, Model model) {
        LOGGER.debug("is executed!");
        model.addAttribute("listAccounts", leagueQueryService.findAll());
        model.addAttribute("account", leagueQueryService.findById(id));

        return "admin-AddLeague";
    }

    @RequestMapping(value = "/league/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable("id") Long id) {
        LOGGER.debug("is executed!");
            leagueCommandService.delete(id);

            return "redirect:/adminAdd";
    }
}
