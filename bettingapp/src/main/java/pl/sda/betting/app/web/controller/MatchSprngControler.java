package pl.sda.betting.app.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.sda.betting.domain.entity.League;
import pl.sda.betting.service.league.query.LeagueQueryService;

import javax.validation.Valid;

@Controller
public class MatchSprngControler {

    private static final Logger LOGGER = LoggerFactory.getLogger(MatchSprngControler.class);

    private final LeagueQueryService leagueQueryService;


    @Autowired
    public MatchSprngControler(LeagueQueryService leagueQueryService) {
        this.leagueQueryService = leagueQueryService;
    }

    @RequestMapping(value = "/admin/add/match", method = RequestMethod.GET)
    public String adminAddMatchPanel(@ModelAttribute("flashAccount") League league, Model model) {
        LOGGER.debug("is executed!");
        model.addAttribute("listLeague", leagueQueryService.findAll());
        model.addAttribute("league", league);

        return "admin-ControlMatch";
    }

    @RequestMapping(value = "/admin/match/save", method = RequestMethod.POST)
    public String saveMatch(@Valid @ModelAttribute("league")League league,@ModelAttribute("secondLeague") League secondLeague,
                            BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        LOGGER.debug("is executed!");

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.league", bindingResult);
            redirectAttributes.addFlashAttribute("flashLeague", league);

            return "redirect:/main";
        }

        /*if (account.getId() == null) {
            matchCommandService.create(league);
        } else {
            matchCommandService.update(league);
        }*/

        return "redirect:/main";
    }
}
