package pl.sda.betting.app.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import pl.sda.betting.domain.entity.League;
import pl.sda.betting.service.league.command.LeagueCommandService;
import pl.sda.betting.service.league.exception.LeagueAlreadyExistsException;
import pl.sda.betting.service.league.exception.LeagueNotFoundException;
import pl.sda.betting.service.league.query.LeagueQueryService;

import java.util.List;


/**
 * Created by RENT on 2017-10-31.
 */
@RestController
public class LeagueControler {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    private final LeagueCommandService leagueCommandService;
    private final LeagueQueryService leagueQueryService;

    @Autowired
    public LeagueControler(LeagueCommandService leagueCommandService, LeagueQueryService leagueQueryService) {
        this.leagueCommandService = leagueCommandService;
        this.leagueQueryService = leagueQueryService;
    }

    @RequestMapping(value = "/league/", method = RequestMethod.POST)
    public ResponseEntity<Void> create(@RequestBody League league, UriComponentsBuilder ucBuilder) {
        LOGGER.debug("is executed!");

        try {
            Long id = leagueCommandService.create(league);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(ucBuilder.path("/league/{id}").buildAndExpand(id).toUri());

            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        } catch (LeagueAlreadyExistsException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/league/", method = RequestMethod.GET)
    public ResponseEntity<List<League>> getAll() {
        LOGGER.debug("is executed!");
        List<League> accountList = leagueQueryService.findAll();
        if (accountList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(accountList, HttpStatus.OK);
    }

    @RequestMapping(value = "/league/{id}", method = RequestMethod.GET)
    public ResponseEntity<League> get(@PathVariable("id") Long id) {
        LOGGER.debug("is executed!");
        try {
            League league = leagueQueryService.findById(id);

            return new ResponseEntity<>(league, HttpStatus.OK);
        } catch (LeagueNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


}
