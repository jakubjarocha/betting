package pl.sda.betting.domain.entity;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;
//import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "LEAGUE")
@Audited
public class League implements Serializable{

    private static final long serialVersionUID = 1194601332130828200L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_LEAGUE")
    private Long id;

    @NotEmpty
    @Column(name = "LEAGUE")
    private String leagueName;

    @NotEmpty
    @Column(name = "DISCIPLINE")
    private String discipline;

    @Column(name = "TEAM_OR_PLAYER")
    private String teamOrplayer;

    private Set<Match> matches;

    public League() {
    }

    public League(String leagueName, String discipline, String teamOrplayer) {
        this.leagueName = leagueName;
        this.discipline = discipline;
        this.teamOrplayer = teamOrplayer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLeagueName() {
        return leagueName;
    }

    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public String getTeamOrplayer() {
        return teamOrplayer;
    }

    public void setTeamOrplayer(String teamOrplayer) {
        this.teamOrplayer = teamOrplayer;
    }

    @OneToMany(mappedBy = "leagueName", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<League> leagues;

    public Set<League> getLeagues() {
        return leagues;
    }

    public void setLeagues(Set<League> leagues) {
        this.leagues = leagues;
    }

    public void addLeagues(League league) {
        if (this.leagues == null) {
            this.leagues = new HashSet<>(0);
        }

        leagues.add(league);
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", leagueName='" + leagueName + '\'' +
                ", dyscypline='" + discipline +
                '}';
    }


}
