package pl.sda.betting.domain.entity;

import org.hibernate.envers.Audited;

import javax.persistence.*;

@Entity
@Table(name = "MATCH")
@Audited
public class Match {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MATCH_ID")
    private Long id;

    @Column(name = "START_MATCH")
    private String startMatch;

    @Column(name = "COURSE")
    private String course;

    @Column(name = "FIRST_TEAM_ID")
    private Long firstTeamId;

    @Column(name = "FIRST_TEAM_SCORE")
    private Long firstTeamScore;

    @Column(name = "SECOND_TEAM_SCORE")
    private Long secondTeamScore;

    @Column(name = "SECOND_TEAM_ID")
    private Long secondTeamId;

    @OneToMany
    @JoinColumn(name = "LEAGUE_ID", nullable = false, foreignKey = @ForeignKey(name = "FK_MATCH_LEAGUE"))
    private League league;

    public Match(String startMatch, String course, Long firstTeamId, Long firstTeamScore, Long secondTeamScore, Long secondTeamId, League league) {
        this.startMatch = startMatch;
        this.course = course;
        this.firstTeamId = firstTeamId;
        this.firstTeamScore = firstTeamScore;
        this.secondTeamScore = secondTeamScore;
        this.secondTeamId = secondTeamId;
        //this.league = league;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStartMatch() {
        return startMatch;
    }

    public void setStartMatch(String startMatch) {
        this.startMatch = startMatch;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public Long getFirstTeamId() {
        return firstTeamId;
    }

    public void setFirstTeamId(Long firstTeamId) {
        this.firstTeamId = firstTeamId;
    }

    public Long getFirstTeamScore() {
        return firstTeamScore;
    }

    public void setFirstTeamScore(Long firstTeamScore) {
        this.firstTeamScore = firstTeamScore;
    }

    public Long getSecondTeamScore() {
        return secondTeamScore;
    }

    public void setSecondTeamScore(Long secondTeamScore) {
        this.secondTeamScore = secondTeamScore;
    }

    public Long getSecondTeamId() {
        return secondTeamId;
    }

    public void setSecondTeamId(Long secondTeamId) {
        this.secondTeamId = secondTeamId;
    }

    /*public League getLeague() {
        return league;
    }

    public void setLeague(League league) {
        this.league = league;
    }*/

    @Override
    public String toString() {
        return "Match{" +
                "id=" + id +
                ", startMatch='" + startMatch + '\'' +
                ", course='" + course + '\'' +
                ", firstTeam='" + firstTeamId + '\'' +
                ", firstTeamScore=" + firstTeamScore +
                ", secondTeamScore=" + secondTeamScore +
                ", secondTeam=" + secondTeamId +
                //", league=" + league +
                '}';
    }
}
