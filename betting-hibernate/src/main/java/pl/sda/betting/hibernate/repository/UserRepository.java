package pl.sda.betting.hibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.betting.domain.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
