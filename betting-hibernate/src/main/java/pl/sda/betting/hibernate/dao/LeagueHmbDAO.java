package pl.sda.betting.hibernate.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import pl.sda.betting.domain.entity.League;

import java.util.List;


/**
 * Created by RENT on 2017-10-30.
 */
public class LeagueHmbDAO {

    private final SessionFactory sessionFactory;

    @Autowired
    public LeagueHmbDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Long create(League league) {
        Session session = sessionFactory.getCurrentSession();
        Long leagueId = (Long) session.save(league);

        return leagueId;
    }

    public void update(League league) {
        Session session = sessionFactory.getCurrentSession();
        session.update(league);
    }

    public League merge(League league) {
        Session session = sessionFactory.getCurrentSession();
        League mergedLeague = (League) session.merge(league);

        return mergedLeague;
    }

    public void delete(League league) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(league);
    }

    public League findById(Long id) {
        Session session = sessionFactory.getCurrentSession();
        League league = session.get(League.class, id);

        return league;
    }

    public List<League> findAll() {
        Session session = sessionFactory.getCurrentSession();
        List<League> leagues = session.createQuery("FROM league", League.class).getResultList();

        return leagues;
    }
}
