package pl.sda.betting.hibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.betting.domain.entity.Match;

import java.util.Map;

/**
 * Created by RENT on 2017-11-13.
 */
@Repository
public interface MatchRepository extends JpaRepository<Match, Long> {
}
