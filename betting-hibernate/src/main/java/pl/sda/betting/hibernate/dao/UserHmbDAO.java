package pl.sda.betting.hibernate.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import pl.sda.betting.domain.entity.User;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import java.util.List;

public class UserHmbDAO {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserHmbDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Long create(User user) {
        Session session = sessionFactory.getCurrentSession();
        Long userId = (Long) session.save(user);

        return userId;
    }

    public void update(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.update(user);
    }

    public User merge(User user) {
        Session session = sessionFactory.getCurrentSession();
       User mergedUser = (User) session.merge(user);

        return mergedUser;
    }

    public void delete(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(user);
    }

    public User findById(Long id) {
        Session session = sessionFactory.getCurrentSession();
        User user= session.get(User.class, id);

        return user;
    }

    public List<User> findAll() {
        Session session = sessionFactory.getCurrentSession();
        List<User> user = session.createQuery("FROM User", User.class).getResultList();

        return user;
    }
}