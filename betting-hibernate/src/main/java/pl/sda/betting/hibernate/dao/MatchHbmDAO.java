package pl.sda.betting.hibernate.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import pl.sda.betting.domain.entity.League;
import pl.sda.betting.domain.entity.Match;

/**
 * Created by RENT on 2017-11-13.
 */
public class MatchHbmDAO {

    private final SessionFactory sessionFactory;

    @Autowired
    public MatchHbmDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Long create(Match match) {
        Session session = sessionFactory.getCurrentSession();
        Long leagueId = (Long) session.save(match);

        return leagueId;
    }

    public void delete(Match match) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(match);
    }
}
