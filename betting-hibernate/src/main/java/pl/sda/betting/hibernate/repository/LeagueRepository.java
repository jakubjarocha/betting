package pl.sda.betting.hibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.betting.domain.entity.League;
/**
 * Created by RENT on 2017-10-30.
 */
@Repository
public interface LeagueRepository extends JpaRepository<League, Long>{
}
