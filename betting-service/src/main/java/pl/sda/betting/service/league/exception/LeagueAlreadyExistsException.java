package pl.sda.betting.service.league.exception;

public class LeagueAlreadyExistsException extends RuntimeException {
    private static final long serialVersionUID = -2097225577357481490L;
}
