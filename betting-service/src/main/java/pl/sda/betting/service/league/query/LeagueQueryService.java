package pl.sda.betting.service.league.query;

        import org.slf4j.Logger;
        import org.slf4j.LoggerFactory;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Service;
        import org.springframework.transaction.annotation.Transactional;
        import pl.sda.betting.domain.entity.League;
        import pl.sda.betting.jdbc.LeagueJdbcDAO;
        import pl.sda.betting.service.league.exception.LeagueNotFoundException;

        import java.util.List;

@Service
@Transactional(readOnly = true)
public class LeagueQueryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LeagueQueryService.class);

    private final LeagueJdbcDAO leagueJdbcDAO;

    @Autowired
    public LeagueQueryService(LeagueJdbcDAO leagueJdbcDAO) {
        this.leagueJdbcDAO = leagueJdbcDAO;
    }

    public List<League> findAll() {
        return leagueJdbcDAO.findAll();
    }

    public League findById(Long id) {
        League league = leagueJdbcDAO.findById(id);
        if (league == null) {
            LOGGER.debug("League with id " + id + " not found.");
            throw new LeagueNotFoundException();
        }

        return league;
    }
}
