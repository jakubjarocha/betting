package pl.sda.betting.service.league.exception;

public class LeagueNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1369368614263620977L;
}
