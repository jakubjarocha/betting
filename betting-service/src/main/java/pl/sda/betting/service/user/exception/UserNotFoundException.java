package pl.sda.betting.service.user.exception;

public class UserNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1369368614263620977L;
}
