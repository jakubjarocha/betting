package pl.sda.betting.service.user.util;

import pl.sda.betting.domain.entity.User;

import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

public class UserStorageUtil {

    public static ConcurrentHashMap<Integer, User> user = new ConcurrentHashMap<>();

    private static User createUser(int id, String firstName, String lastName,String email, Date birthdate) {
        User user = new User(firstName, lastName, email , birthdate);
        user.setId(id);

        return user;
    }
}
