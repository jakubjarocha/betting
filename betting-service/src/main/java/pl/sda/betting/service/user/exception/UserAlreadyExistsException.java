package pl.sda.betting.service.user.exception;

public class UserAlreadyExistsException extends RuntimeException {
    private static final long serialVersionUID = -2097225577357481490L;
}