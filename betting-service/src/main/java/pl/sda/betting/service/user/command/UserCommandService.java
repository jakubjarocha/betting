package pl.sda.betting.service.user.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.betting.domain.entity.User;
import pl.sda.betting.hibernate.repository.UserRepository;
import pl.sda.betting.service.user.exception.UserNotFoundException;

@Service
@Transactional
public class UserCommandService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserCommandService.class);

    private final UserRepository userRepository;

    @Autowired
    public UserCommandService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Long create(User user) {
        userRepository.save(user);

        return Long.valueOf(user.getId());
    }

    public void update(User user) {
        User dbUser = userRepository.findOne((long) user.getId());
        if (dbUser == null) {
            LOGGER.debug("User with id " + user.getId() + " not found.");
            throw new UserNotFoundException();
        }
        dbUser.setFirstName(user.getFirstName());
        dbUser.setLastName(user.getLastName());
        dbUser.setBirthdate(user.getBirthdate());
        dbUser.setEmail(user.getEmail());
    }

    public void delete(Long id) {
        User user = userRepository.findOne(id);
        if (user == null) {
            LOGGER.debug("User with id " + id + " not found.");
            throw new UserNotFoundException();
        }

        userRepository.delete(user);
    }
}


