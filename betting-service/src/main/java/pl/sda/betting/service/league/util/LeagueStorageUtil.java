package pl.sda.betting.service.league.util;

import pl.sda.betting.domain.entity.League;

import java.util.concurrent.ConcurrentHashMap;

public class LeagueStorageUtil {

    public static ConcurrentHashMap<Long, League> leagues = new ConcurrentHashMap<>();

    static {
        leagues.put(1L, createLeague(1L, "NBA", "BASKETBALL", "Lakers"));
        leagues.put(2L, createLeague(2L, "NBA", "BASKETBALL", "Knicks"));
        leagues.put(3L, createLeague(3L, "NBA", "BASKETBALL", "Cavaliers"));
    }

    private static League createLeague(Long id, String leagueName, String discipline, String teamOrplayer) {
        League league = new League(leagueName, discipline, teamOrplayer);
        league.setId(id);

        return league;
    }
}
