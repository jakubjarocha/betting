package pl.sda.betting.service.league.command;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.betting.domain.entity.League;
import pl.sda.betting.hibernate.repository.LeagueRepository;
import pl.sda.betting.service.league.exception.LeagueNotFoundException;

import javax.transaction.Transactional;

@Service
@Transactional
public class LeagueCommandService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LeagueCommandService.class);
    LeagueRepository leagueRepository;

    @Autowired
    public LeagueCommandService(LeagueRepository leagueRepository) {
        this.leagueRepository = leagueRepository;
    }

    public Long create(League league) {
        leagueRepository.save(league);
        return league.getId();
    }

    public void update(League league) {
        League dbLeague = leagueRepository.findOne(league.getId());
        if (dbLeague == null) {
            LOGGER.debug("Account with id " + league.getId() + " not found.");
            throw new LeagueNotFoundException();
        }
        dbLeague.setDiscipline(league.getDiscipline());
        dbLeague.setLeagueName(league.getLeagueName());
        dbLeague.setTeamOrplayer(league.getTeamOrplayer());
    }

    public void delete(Long id) {
        League league = leagueRepository.findOne(id);
        if (league == null) {
            LOGGER.debug("Account with id " + id + " not found.");
            throw new LeagueNotFoundException();
        }

        leagueRepository.delete(league);
    }
}
