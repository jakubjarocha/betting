package pl.sda.betting.service.user.query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.betting.domain.entity.User;
import pl.sda.betting.jdbc.UserJdbcDAO;
import pl.sda.betting.service.user.exception.UserNotFoundException;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class UserQueryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserQueryService.class);

    private final UserJdbcDAO userJdbcDAO;

    @Autowired
    public UserQueryService(UserJdbcDAO userJdbcDAO) {
        this.userJdbcDAO = userJdbcDAO;
    }

    public List<User> findAll() {
        return userJdbcDAO.findAll();
    }

    public User findById(Integer id) {
        User user = userJdbcDAO.findById(id);
        if (user == null) {
            LOGGER.debug("User with id " + id + " not found.");
            throw new UserNotFoundException();
        }

        return user;
    }
}
