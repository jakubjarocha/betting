package pl.sda.betting.service.match.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.betting.domain.entity.League;
import pl.sda.betting.domain.entity.Match;
import pl.sda.betting.hibernate.repository.MatchRepository;
import pl.sda.betting.service.league.command.LeagueCommandService;
import pl.sda.betting.service.league.exception.LeagueNotFoundException;

/**
 * Created by RENT on 2017-11-13.
 */
@Service
@Transactional
public class MatchCommandService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MatchCommandService.class);
    private MatchRepository matchRepository;

    @Autowired
    public MatchCommandService(MatchRepository matchRepository) {
        this.matchRepository = matchRepository;
    }

    public Long create(Match match) {
        matchRepository.save(match);
        return match.getId();
    }

    public void delete(Long id) {
        Match match = matchRepository.findOne(id);
        if (match == null) {
            LOGGER.debug("Account with id " + id + " not found.");
            throw new LeagueNotFoundException();
        }

        matchRepository.delete(match);
    }
}
